//modulos internos 
const jwt = require("jsonwebtoken");

//crear la funcion de middelwares
function auth (req, res, next){
    let jwtToken = req.header("Authorization");
    jwtToken = jwtToken.split(" ")[1];

    //validar si hay un token 
    if(!jwtToken) return res.status(400).send("No existe token para validar");

    //si existe un token 
    try{
        const payload = jwt.verify(jwtToken, "clave");
        req.usuario = payload;
        next();
    }catch(error){
        res.status(400).send("token no valido sin autorización");

    }
}

//exports
module.exports = auth;