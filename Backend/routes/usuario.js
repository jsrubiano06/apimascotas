//modulos internos 
const express = require("express")
const router = express.Router();

//modulos propios
const { Usuario } = require("../model/usuario");

//rutas
router.post("/", async (req, res) => {
    //revisar el usuario que existe 
    let usuario = await Usuario.findOne({ correo: req.body.correo });
    //si el usuario existe en la bd 
    if (usuario) return res.status(400).send("El usuario existe en la bd");
    //si el usuario no existe 
    usuario = new Usuario({
        nombre: req.body.nombre,
        correo: req.body.correo,
        password: req.body.password,
    });
    //guardar el usuario en la base de datos y generamos el JWT 
    const result = await usuario.save();
    const jwtToken = usuario.generateJWT();
    res.status(200).send({ jwtToken });
});
//esports
module.exports = router;
