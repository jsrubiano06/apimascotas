//modulos internos 
const express = require("express");
const router = express.Router();
//modulos propios
const { Usuario } = require("../model/usuario");
const { Mascota }= require("../model/mascota");

const auth = require("../middelware/auth")
//rutas 
// insertando una tarea 
router.post("/", auth, async(req, res) => {
    //tomar el usuario si existe 
    const usuario = await Usuario.findById(req.usuario._id);
    //si el usuario no existe 
    if(!usuario) return res.status(400).send("El usuario no existe");
      //revisar que la mascota no exita todavia
    let mascota = await Mascota.findOne({ tipo: req.body.tipo});
     if(mascota) return res.status(400).send("La mascota ya existe en la BD ");
    //SI LA MASCOTA NO EXITE 
    //objeto
    mascota = new Mascota({
        idUsuario: usuario._id,
        nombre: req.body.nombre,
        tipo: req.body.tipo,
        descripcion: req.body.descripcion,
        
    });
    //GUARDAR LA MASCOTA
    const result = await mascota.save();
    res.status(200).send(result);
});
//listar las mascota del usuario registrado 
router.get("/lista", auth, async(req,res) =>{
    //tomar el usuario que esta en sesion 
    const usuario = await Usuario.findById(req.usuario._id);
    //pregutar si el usuario existe y no existe
    //no existe el usuario
    if(!usuario) return res.status(400).send("El usuario no existe en la base de datos");
    //si el usuario existe tomamos todas sus tareas
    const mascota = await Mascota.find({idUsuario: req.usuario._id});
    res.send(mascota);

});

//editar mascotas de usuario registrado 
router.put("/", auth, async(req, res) => {
    const usuario = await Usuario.findById(req.usuario._id);
    //si el usario no existe
    if(!usuario) return res.status(400).send("El usuario no existe en la base de datos");
    //si el usuario existe hacemos el update 
    const mascota = await Mascota.findByIdAndUpdate(
        req.body._id,
        {
            idUsuario: usuario._id,
            nombre: req.body.nombre,
            tipo: req.body.tipo,
            descripcion: req.body.descripcion, 
        },{
            new:true,
        }
        
    );
    if(!mascota) return res.status(400).send("No Tiene mascotas Registradas")
    //si exnten mascotas
    res.status(200).send(mascota);
});
//eliminar una mascota del usuario logeado 
router.delete("/:_id", auth, async(req, res) =>{
    //tomar el usuario logeado
    const usuario = await Usuario.findById(req.usuario._id);
    //si el usuario no existe 
    if(!usuario) return res.status(400).send("El usuario no existe en la bd");
    //si el usuario existe hacememos el update o delete
    const mascota = await Mascota.findByIdAndDelete(req.params._id);
    //si no encuentra la mascota
    if(!mascota) return res.status(400).send("No hay Mascotas asignadas");
    //si encuentra la tarea 
    res.status(200).send({message:"Mascota Eliminada"});
});
/*-----------------------------fin de rutas*/
//exports
module.exports = router;