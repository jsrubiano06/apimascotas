//modulos internos 
const express = require("express");
const router = express.Router();

//modulos propios
const { Usuario } = require("../model/usuario");

//rutas
router.post("/", async (req, res) => {
    //revisar el usuario que existe 
    let usuario = await Usuario.findOne({ correo: req.body.correo });
    //si el usuario existe en la bd 
    if (!usuario) return res.status(400).send("Correo o contraseña son invalidos");
    //si existe el usuario validar la contraseña
    if(usuario.password !== req.body.password) return res.status(400).send("Correo o contraseña son invalidos");

    const jwtToken = usuario.generateJWT();
    res.status(200).send({jwtToken});
});
//exports
module.exports = router; 