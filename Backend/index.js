//modulos internos 
const express = require("express");
const mongoose = require("mongoose");
//modulos creados
const usuario = require("./routes/usuario");
const auth = require("./routes/auth");
const mascota = require("./routes/mascota");
//app
const app = express();
app.use(express.json());
//ruta de mi api
app.use("/api/usuario", usuario);
app.use("/api/auth",auth);
app.use("/api/mascota", mascota);
//puerto de ejecucion 
const port = process.env.PORT || 3000;
app.listen(port, ()=>console.log("Ejecutando el puerto: " +port))

mongoose.connect("mongodb://localhost/apimascotas",{
    useNewUrlParser:true,
    useFindAndModify:false,
    useCreateIndex:true,
    useUnifiedTopology:true,
})
.then(() => console.log("conexion a mongo exitosa"))
.catch((error) => console.log("conexion a mongo OFF" + error));  
