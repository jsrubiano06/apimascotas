//modulos internos  variable para traer los modulo
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken") //json web token
//definir el esquema de mi coleccion
const esquemaUsuario = new mongoose.Schema({
    nombre: String,
    correo: String,
    password: String,
});
//generar el Jwt
esquemaUsuario.methods.generateJWT = function () {
    return jwt.sign(
        {
        _id: this._id,
        nombre: this.nombre,
        correo: this.correo,
    }, "clave"
    );

};

//exports f
const Usuario = mongoose.model("usuario", esquemaUsuario);
module.exports.Usuario = Usuario;
module.exports.esquemaUsuario = esquemaUsuario;