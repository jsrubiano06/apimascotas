//modulo interno 
const mongoose = require("mongoose");

//esquema mascota 
const esquemaMascota = new mongoose.Schema({
    idUsuario:String,
    nombre:String,
    tipo:String,
    descripcion:String,
});
//exports
const Mascota = mongoose.model("mascota", esquemaMascota);
module.exports.Mascota = Mascota;